@extends('layouts.app')

@section('content')
    <thread-page :initial-reply-count="{{ $thread->replies_count }}" inline-template>
        <div class="container">
            <div class="row">
                <div class="col-9">

                    <div class="card my-4 bg-light">
                        <div class="card-header">
                            <div class="d-flex justify-content-between align-items-center">
                                <img src="{{ asset($thread->author->avatar()) }}" height="27" width="27">
                                <h5 class="m-0 mr-auto pl-3">{{ $thread->title }}</h5>
                                <div>
                                    @can('delete', $thread)
                                        <form action="{{ $thread->path() }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-link btn-sm"><i class="fa fa-fw fa-times"></i></button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {{ $thread->body }}
                        </div>
                    </div>

                    <replies @deleted="replyCount--" @added="replyCount++"></replies>

                    <new-reply class="my-3" :thread="{{ $thread->id }}" endpoint="{{ $thread->path('replies') }}"></new-reply>

                </div>
                <div class="col-3">

                    <div class="card my-4 bg-light">
                        <div class="card-body">
                            This thread was created {{ $thread->created_at->diffForHumans() }} by <a href="{{ $thread->author->profile() }}">{{ $thread->author->name }}</a> and currently has @{{ replyCount }} replies.
                        </div>
                    </div>

                    <subscribe-button :active="@json($thread->isSubscribed)" endpoint="{{ $thread->path('subscriptions') }}"></subscribe-button>

                </div>
            </div>
        </div>
    </thread-page>
@endsection
