@forelse($threads as $thread)
    @component('layouts.components.card')
        @slot('header')
            <div class="d-flex justify-content-between align-items-center">
                <img class="border border-dark rounded" src="{{ asset($thread->author->avatar()) }}" height="45" width="45">
                <div class="mr-auto pl-2">
                    @if (auth()->check() && $thread->hasUpdatesFor(auth()->user()))
                        <h4 class="m-0 font-weight-bold">
                            <a href="{{ $thread->path() }}">{{ $thread->title }}</a>
                        </h4>
                    @else
                        <h4 class="m-0"><a href="{{ $thread->path() }}">{{ $thread->title }}</a></h4>
                    @endif
                    <div>Posted by: <a href="{{ $thread->author->profile() }}">{{ $thread->author->name }}</a></div>
                </div>
                <strong><a href="{{ $thread->path() }}">{{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}</a></strong>
            </div>
        @endslot
        <div class="card-body">
            {{ str_limit($thread->body, 350) }}
        </div>
    @endcomponent
@empty
    <div class="my-5">
        <h3 class="py-5 m-0 text-muted text-center">There are no threads at this time :(</h3>
    </div>
@endforelse
