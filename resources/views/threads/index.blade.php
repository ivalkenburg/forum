@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('threads._list', ['threads' => $threads])
                <div>
                    {{ $threads->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection