@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">

                <div class="card my-4">
                    <div class="card-header">
                        Create Thread
                    </div>
                    <form action="{{ route('threads.store') }}" method="POST">
                        <div class="card-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="title">Title:</label>
                                    <input type="text" name="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" value="{{ old('title') }}" required>
                                    @if($errors->has('title'))
                                        <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="channel_id">Channel:</label>
                                    <select name="channel_id" id="channel_id" class="form-control{{ $errors->has('channel_id') ? ' is-invalid' : '' }}" required>
                                        <option value="" {{ !old('channel_id') ? 'selected ' : '' }}disabled>Choose a channel</option>
                                        @foreach($channels as $channel)
                                            <option value="{{ $channel->id }}"{{ old('channel_id') == $channel->id ? ' selected' : '' }}>{{ studly_case($channel->name) }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('channel_id'))
                                        <div class="invalid-feedback">{{ $errors->first('channel_id') }}</div>
                                    @endif
                                </div>
                                <div>
                                    <label for="body">Body:</label>
                                    <textarea name="body" id="body" rows="8" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" required>{{ old('body') }}</textarea>
                                    @if($errors->has('body'))
                                        <div class="invalid-feedback">{{ $errors->first('body') }}</div>
                                    @endif
                                </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="button" class="btn btn-link" onclick="window.history.back()">Back</button>
                            <button type="submit" class="btn btn-primary">Post Thread</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
