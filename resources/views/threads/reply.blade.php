<reply :attributes="{{ $reply }}" v-cloak inline-template>
    <div id="reply-{{ $reply->id }}" class="card my-4" v-if="visible">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div>
                <i class="fa fa-reply fa-fw text-muted mr-1"></i> <a href="{{ $reply->author->profile() }}">{{ $reply->author->name }}</a> replied {{ $reply->created_at->diffForHumans() }}.
            </div>
            <div>
                @can('update', $reply)
                    <button class="btn btn-sm btn-link" @click="editing = !editing" type="button"><i class="fa fa-edit fa-fw"></i></button>
                @endcan
                @can('delete', $reply)
                    <button class="btn btn-sm btn-link" @click.prevent="destroy" type="button"><i class="fa fa-times fa-fw"></i></button>
                @endcan
                @auth
                    <favorite :reply="{{ $reply }}"></favorite>
                @endauth
            </div>
        </div>
        <div class="card-body">
            <template v-if="editing">
                <div class="form-group">
                    <textarea class="form-control" v-model="body" rows="7"></textarea>
                </div>
                <div class="d-flex justify-content-between">
                    <button class="btn btn-sm btn-link" @click="editing = false">Cancel</button>
                    <button class="btn btn-sm btn-primary" @click="update">Update</button>
                </div>
            </template>
            <div v-text="body" v-else></div>
        </div>
    </div>
</reply>