@extends('layouts.app')

@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-end">
                    <h1 class="mb-0">{{ $profileUser->name }}</h1>
                    <span class="text-muted">Registered {{ $profileUser->created_at->diffForHumans() }}.</span>
                </div>
                @can('update', $profileUser)
                    <form method="POST" action="{{ route('profile.avatar.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" name="avatar">
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </form>
                @endcan
                <img src="{{ asset($profileUser->avatar()) }}" width="50" height="50">
                <hr>
                @forelse($activities as $activity)
                    @includeIf("profiles.activities.{$activity->type}")
                @empty
                    <p class="my-5 text-center text-muted">There is no activity for this user yet.</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection