@component('layouts.components.card')
    @slot('header')
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <i class="fa fa-comments-o mr-1 text-muted"></i> Created: <a href="{{ $activity->subject->path() }}"><strong>{{ $activity->subject->title }}</strong></a>
            </div>
            <div class="text-muted">
                {{ $activity->subject->created_at->diffForHumans() }}
            </div>
        </div>
    @endslot
    <div class="card-body">
        {{ $activity->subject->body }}
    </div>
@endcomponent