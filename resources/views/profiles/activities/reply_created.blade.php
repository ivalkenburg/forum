@component('layouts.components.card')
    @slot('header')
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <i class="fa fa-reply mr-1 text-muted"></i> Replied to: <a href="{{ $activity->subject->thread->path() }}"><strong>{{ $activity->subject->thread->title }}</strong></a>
            </div>
            <div class="text-muted">
                {{ $activity->subject->created_at->diffForHumans() }}
            </div>
        </div>
    @endslot
    <div class="card-body">
        {{ $activity->subject->body }}
    </div>
@endcomponent