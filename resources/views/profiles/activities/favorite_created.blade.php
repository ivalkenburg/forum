@component('layouts.components.card')
    @slot('header')
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <i class="fa fa-heart mr-1 text-muted"></i> Favorited reply by <strong><a href="{{ $activity->subject->favorited->author->profile() }}">{{ $activity->subject->favorited->author->name }}</a></strong> on <strong><a href="{{ $activity->subject->favorited->path() }}">{{ $activity->subject->favorited->thread->title }}</a></strong>
            </div>
            <div class="text-muted">
                {{ $activity->subject->created_at->diffForHumans() }}
            </div>
        </div>
    @endslot
    <div class="card-body">
        {{ $activity->subject->favorited->body }}
    </div>
@endcomponent