<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.name') }}</a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="threads" aria-haspopup="true" aria-expanded="false">Browse</a>
                        <div class="dropdown-menu" aria-labelledby="threads">
                            <a class="dropdown-item" href="{{ route('threads.index') }}">All Threads</a>
                            <a class="dropdown-item" href="{{ route('threads.index', ['popularity' => 1]) }}">By Popularity</a>
                            <a class="dropdown-item" href="{{ route('threads.index', ['unanswered' => 1]) }}">Unanswered</a>
                            @auth
                                <a class="dropdown-item" href="{{ route('threads.index', ['by' => auth()->user()->name]) }}">My
                                    Threads</a>
                            @endauth
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Channels</a>
                        <div class="dropdown-menu">
                            @foreach($channels as $channel)
                                <a class="dropdown-item" href="{{ $channel->path() }}">{{ studly_case($channel->name) }}</a>
                            @endforeach
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('threads.create') }}">New Thread</a>
                        </li>
                        <li>
                            <user-notifications></user-notifications>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">{{ auth()->user()->name }}</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ auth()->user()->profile() }}" class="dropdown-item">Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Register</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</header>