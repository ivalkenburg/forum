import Vue from 'vue';

Vue.mixin({
    methods: {
        auth(callback) {
            const user = window.app.user;

            if (!user) return false;
            if (user.is_admin) return true;

            return callback(user);
        }
    },
    computed: {
        loggedIn() {
            return !!window.app.user;
        }
    }
});