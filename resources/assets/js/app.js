import Vue from 'vue';

import './plugins';
import './components';
import './pages';

export default new Vue().$mount('#app');
