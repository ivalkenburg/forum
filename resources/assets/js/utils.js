export function formatFormErrors(response) {
    let errors = {};

    Object.keys(response.data.errors).forEach(field => {
        errors[field] = response.data.errors[field][0];
    });

    return errors;
}

export function flash(body, color = 'success') {
    window.events.$emit('flash', { body, color });
};