import Vue from 'vue';

import Flash from './Flash';
import Pagination from './Pagination';
import UserNotifications from './UserNotifications';

Vue.component(Flash.name, Flash);
Vue.component(Pagination.name, Pagination);
Vue.component(UserNotifications.name, UserNotifications);
