<?php

use Faker\Generator as Faker;

$factory->define(\Illuminate\Notifications\DatabaseNotification::class, function (Faker $faker) {
    return [
        'id'              => \Illuminate\Support\Str::uuid(),
        'type'            => \App\Notifications\ThreadHasNewReply::class,
        'notifiable_id'   => function () {
            return auth()->id() ?: create(\App\Models\User::class)->id;
        },
        'notifiable_type' => \App\Models\User::class,
        'data'            => ['foo' => 'bar']
    ];
});
