<?php

use App\Models\Channel;
use Faker\Generator as Faker;

$factory->define(Channel::class, function(Faker $faker) {
    $name = $faker->unique()->word;

    return [
        'name' => $name,
        'slug' => str_slug($name)
    ];
});
