<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email'    => 'lolrogii@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => true
        ]);
    }
}
