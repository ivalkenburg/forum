<?php

use App\Models\Channel;
use App\Models\Thread;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ThreadsTableSeeder extends Seeder
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = Channel::pluck('id')->toArray();

        for($i = 0; $i < 50; $i++) {
            factory(Thread::class)->create(['channel_id' => $this->faker->randomElement($channels)]);
        }
    }
}
