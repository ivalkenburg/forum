<?php

namespace App\Policies;

use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThreadPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) return true;
    }

    /**
     * @param User   $user
     * @param Thread $thread
     * @return bool
     */
    public function view(User $user, Thread $thread)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return auth()->check();
    }

    /**
     * @param User   $user
     * @param Thread $thread
     * @return bool
     */
    public function update(User $user, Thread $thread)
    {
        return $user->owns($thread);
    }

    /**
     * @param User   $user
     * @param Thread $thread
     * @return bool
     */
    public function delete(User $user, Thread $thread)
    {
        return $user->owns($thread);
    }
}
