<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) return true;
    }

    /**
     * @param User $authenticatedUser
     * @param User $user
     * @return bool
     */
    public function update(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->is($user);
    }
}
