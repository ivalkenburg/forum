<?php

namespace App\Policies;

use App\Models\Reply;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) return true;
    }

    /**
     * @param User  $user
     * @param Reply $reply
     * @return bool
     */
    public function view(User $user, Reply $reply)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return auth()->check();
    }

    /**
     * @param User  $user
     * @param Reply $reply
     * @return bool
     */
    public function update(User $user, Reply $reply)
    {
        return $user->owns($reply);
    }

    /**
     * @param User  $user
     * @param Reply $reply
     * @return bool
     */
    public function delete(User $user, Reply $reply)
    {
        return $user->owns($reply);
    }
}
