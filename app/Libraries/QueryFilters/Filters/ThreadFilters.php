<?php

namespace App\Libraries\QueryFilters\Filters;

use App\Libraries\QueryFilters\BaseFilters;
use App\Models\User;

class ThreadFilters extends BaseFilters
{
    protected $filters = ['by', 'popularity', 'unanswered'];

    /**
     * @param $username
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        $this->builder->where('user_id', $user->id);
    }

    protected function popularity()
    {
        $this->builder->getQuery()->orders = [];

        $this->builder->orderBy('replies_count', 'desc');
    }

    protected function unanswered()
    {
        $this->builder->doesntHave('replies');
    }
}