<?php

namespace App\Libraries\QueryFilters;

use Illuminate\Http\Request;

abstract class BaseFilters
{
    protected $request;
    protected $builder;
    protected $filters = [];

    /**
     * BaseFilters constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $builder
     * @return mixed
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            $this->$filter($value);
        }

        return $this->builder;
    }

    /**
     * @return array
     */
    protected function getFilters()
    {
        return $this->request->only($this->filters);
    }
}