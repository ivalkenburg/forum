<?php

namespace App\Libraries\Spam;

use App\Libraries\Spam\Filters\InvalidKeywords;
use App\Libraries\Spam\Filters\RepeatingCharacters;

class Spam
{
    protected $filters = [
        InvalidKeywords::class,
        RepeatingCharacters::class
    ];

    /**
     * @param $text
     * @return bool
     */
    public function detect($text)
    {
        foreach($this->filters as $filter) {
            app($filter)->detect($text);
        }

        return false;
    }
}