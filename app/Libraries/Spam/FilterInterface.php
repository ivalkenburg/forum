<?php

namespace App\Libraries\Spam;


interface FilterInterface
{
    public function detect($text);
}