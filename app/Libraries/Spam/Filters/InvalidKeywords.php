<?php

namespace App\Libraries\Spam\Filters;

use App\Libraries\Spam\FilterInterface;
use App\Libraries\Spam\SpamDetectedException;

class InvalidKeywords implements FilterInterface
{
    protected $keywords = [
        'yahoo customer support',
        '/^hello world$/'
    ];

    /**
     * @param $text
     * @throws SpamDetectedException
     */
    public function detect($text)
    {
        foreach ($this->keywords as $keyword) {
            if (preg_match('/^\/.+\/[a-z]*$/i', $keyword)) {
                if (preg_match($keyword, $text)) {
                    throw new SpamDetectedException('Invalid text detected.');
                }
            } else {
                if (stripos($text, $keyword) !== false) {
                    throw new SpamDetectedException('Invalid text detected.');
                }
            }
        }
    }
}