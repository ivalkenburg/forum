<?php

namespace App\Libraries\Spam\Filters;

use App\Libraries\Spam\FilterInterface;
use App\Libraries\Spam\SpamDetectedException;

class RepeatingCharacters implements FilterInterface
{
    /**
     * @param $text
     * @throws SpamDetectedException
     */
    public function detect($text)
    {
        if (preg_match('/(.)\\1{4,}/', $text)) {
            throw new SpamDetectedException('Excessive repeating characters detected.');
        }
    }
}