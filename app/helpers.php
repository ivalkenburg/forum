<?php

use Illuminate\Validation\ValidationException;

if (!function_exists('formatValidationException')) {

    /**
     * @param      $field
     * @param null $message
     * @return ValidationException
     */
    function formatValidationException($field, $message = null)
    {
        if (is_array($field)) {
            return ValidationException::withMessages($field);
        } else {
            return ValidationException::withMessages([
                $field => [$message]
            ]);
        }
    }
}

