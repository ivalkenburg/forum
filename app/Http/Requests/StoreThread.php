<?php

namespace App\Http\Requests;

use App\Rules\IsNotSpam;
use Illuminate\Foundation\Http\FormRequest;

class StoreThread extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !$this->userJustPosted();
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedAuthorization()
    {
        throw formatValidationException('body', 'Must wait at least 1 minute between creating threads.');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'exists:channels,id'],
            'title'      => ['required', new IsNotSpam],
            'body'       => ['required', new IsNotSpam]
        ];
    }

    /**
     * @return bool
     */
    protected function userJustPosted()
    {
        return optional($this->user()->fresh()->lastThread)->wasJustCreated() ?: false;
    }
}
