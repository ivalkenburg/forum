<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;

class UserNotificationController extends Controller
{
    /**
     * UserNotificationController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $notifications = auth()->user()->unreadNotifications;

        return response()->json($notifications, Response::HTTP_OK);
    }

    /**
     * @param $user
     * @param $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll($user)
    {
        $updated = auth()->user()->notifications()->whereNull('read_at')->update(['read_at' => now()]);

        return $updated
            ? response()->json(null, Response::HTTP_OK)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param $user
     * @param $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($user, $notification)
    {
        auth()->user()->notifications()->findOrFail($notification)->markAsRead();

        return response()->json(null, Response::HTTP_OK);
    }
}
