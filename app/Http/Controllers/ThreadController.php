<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreThread;
use App\Libraries\QueryFilters\Filters\ThreadFilters;
use App\Models\Channel;
use App\Models\Thread;

class ThreadController extends Controller
{
    /**
     * ThreadController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * @param Channel       $channel
     * @param ThreadFilters $filters
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Channel $channel, ThreadFilters $filters)
    {
        $threads = $this->getThreads($channel, $filters);

        if (request()->expectsJson()) return $threads;

        return view('threads.index', compact('threads'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * @param StoreThread $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreThread $request)
    {
        $thread = auth()->user()->threads()->create(
            $request->only('channel_id', 'title', 'body')
        );

        return $this->flashTo($thread->path(), 'Thread created!');
    }

    /**
     * @param        $channel
     * @param Thread $thread
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($channel, Thread $thread)
    {
        if (auth()->check()) {
            auth()->user()->read($thread);
        }

        return view('threads.show', compact('thread'));
    }

    /**
     * @param        $channel
     * @param Thread $thread
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($channel, Thread $thread)
    {
        $this->authorize('delete', $thread);

        $thread->delete();

        return $this->flashTo(route('threads.index'), 'Thread deleted!');
    }

    /**
     * @param Channel       $channel
     * @param ThreadFilters $filters
     * @return mixed
     */
    protected function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);

        if ($channel->exists) {
            $threads = $threads->where('channel_id', $channel->id);
        }

        $threads = $threads->paginate(10);

        return $threads;
    }
}
