<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Symfony\Component\HttpFoundation\Response;

class ThreadSubscriptionController extends Controller
{
    /**
     * ThreadSubscriptionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param        $channelId
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($channelId, Thread $thread)
    {
        $subscription = $thread->subscribe();

        return $subscription->exists
            ? response()->json(null, Response::HTTP_CREATED)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param        $channelId
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($channelId, Thread $thread)
    {
        return $thread->unsubscribe()
            ? response()->json(null, Response::HTTP_OK)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
