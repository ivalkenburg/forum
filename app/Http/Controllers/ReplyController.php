<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreReply;
use App\Http\Requests\UpdateReply;
use App\Models\Reply;
use App\Models\Thread;
use Symfony\Component\HttpFoundation\Response;

class ReplyController extends Controller
{
    /**
     * ReplyController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * @param        $channel
     * @param Thread $thread
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index($channel, Thread $thread)
    {
        return response()->json($thread->replies()->paginate(10), Response::HTTP_OK);
    }

    /**
     * @param            $channel
     * @param Thread     $thread
     * @param StoreReply $request
     * @return Reply|\Illuminate\Http\RedirectResponse
     */
    public function store($channel, Thread $thread, StoreReply $request)
    {
        $this->authorize('create', Reply::class);

        $reply = $thread->addReply([
            'user_id' => auth()->id(),
            'body'    => $request->body
        ])->load('author');

        return $reply->exists
            ? response()->json($reply, Response::HTTP_CREATED)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Reply $reply
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('delete', $reply);

        return $reply->delete()
            ? response()->json(null, Response::HTTP_OK)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Reply $reply
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function favorite(Reply $reply)
    {
        $status = $reply->favorite();

        return response()->json(compact('status'), Response::HTTP_OK);
    }

    /**
     * @param Reply       $reply
     * @param UpdateReply $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Reply $reply, UpdateReply $request)
    {
        $this->authorize('update', $reply);

        return $reply->update($request->only('body'))
            ? response()->json($reply, Response::HTTP_OK)
            : response()->json(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
