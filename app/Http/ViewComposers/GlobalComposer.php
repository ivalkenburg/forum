<?php

namespace App\Http\ViewComposers;

use App\Models\Channel;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class GlobalComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('channels', Cache::rememberForever('channels', function () {
            return Channel::all();
        }));
    }
}