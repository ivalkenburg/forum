<?php

namespace App\Rules;

use App\Libraries\Spam\Spam;
use App\Libraries\Spam\SpamDetectedException;
use Illuminate\Contracts\Validation\Rule;

class IsNotSpam implements Rule
{
    protected $spam;
    protected $message = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->spam = new Spam;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $this->spam->detect($value);
        } catch (SpamDetectedException $exception) {
            $this->message = $exception->getMessage();

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?: trans('validation.spam');
    }
}
