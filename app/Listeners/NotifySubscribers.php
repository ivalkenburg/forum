<?php

namespace App\Listeners;

use App\Events\ThreadReceivedReply;
use App\Models\ThreadSubscription;

class NotifySubscribers
{
    /**
     * Handle the event.
     *
     * @param ThreadReceivedReply $event
     * @return void
     */
    public function handle(ThreadReceivedReply $event)
    {
        $event->thread
            ->subscriptions()
            ->where('user_id', '!=', $event->reply->user_id)
            ->each(function (ThreadSubscription $subscription) use ($event) {
                $subscription->notify($event->reply);
            });
    }
}
