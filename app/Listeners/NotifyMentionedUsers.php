<?php

namespace App\Listeners;

use App\Events\ThreadReceivedReply;
use App\Models\User;
use App\Notifications\UserMentioned;

class NotifyMentionedUsers
{
    /**
     * Handle the event.
     *
     * @param ThreadReceivedReply $event
     * @return void
     */
    public function handle(ThreadReceivedReply $event)
    {
        $event->reply->mentionedUsers()->each(function (User $user) use ($event) {
            $user->notify(new UserMentioned($event->thread, $event->reply));
        });
    }
}
