<?php

namespace App\Events;

use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Queue\SerializesModels;

class ThreadReceivedReply
{
    use SerializesModels;

    public $thread;
    public $reply;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Thread $thread, Reply $reply)
    {
        $this->thread = $thread;
        $this->reply  = $reply;
    }
}
