<?php

namespace App\Traits;

use App\Models\Favorite;

trait Favorable
{
    protected static function bootFavorable()
    {
        static::deleting(function ($model) {
            $model->favorites->each->delete();
        });
    }

    /**
     * @return bool
     */
    public function favorite()
    {
        if (!$this->favorited) {
            $this->favorites()->create(['user_id' => auth()->id()]);

            return true;
        } else {
            $this->favorites()->where('user_id', auth()->id())->get()->each->delete();

            return false;
        }
    }

    /**
     * @return mixed
     */
    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }

    /**
     * @return bool
     */
    public function getFavoritedAttribute()
    {
        return !!$this->favorites()->where('user_id', auth()->id())->count();
    }
}