<?php

namespace App\Traits;

trait SendsResponses
{
    /**
     * @param        $route
     * @param        $message
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function flashTo($route, $message, $type = 'success')
    {
        return redirect($route)->with('flash', compact('message', 'type'));
    }

    /**
     * @param        $message
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function flashBack($message, $type = 'success')
    {
        return back()->with('flash', compact('message', 'type'));
    }

}