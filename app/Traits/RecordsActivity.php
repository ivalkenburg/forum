<?php

namespace App\Traits;

use App\Models\Activity;

trait RecordsActivity
{
    protected static function bootRecordsActivity()
    {
        if (auth()->guest()) return;

        foreach (static::getRecordEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });
        }

        static::deleting(function ($model) {
            $model->activities()->delete();
        });
    }

    /**
     * @return array
     */
    protected static function getRecordEvents()
    {
        return static::$recordEvents ?? [];
    }

    /**
     * @param $event
     * @return mixed
     */
    public function recordActivity($event)
    {
        return $this->activities()->create([
            'user_id' => auth()->id(),
            'type'    => $this->getActivityType($event)
        ]);
    }

    /**
     * @return mixed
     */
    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    /**
     * @param $event
     * @return string
     */
    protected function getActivityType($event)
    {
        $model = strtolower((new \ReflectionClass($this))->getShortName());

        return "{$model}_{$event}";
    }
}