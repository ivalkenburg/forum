<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];

    /**
     * @param User $user
     * @param int  $take
     * @return mixed
     */
    public static function feed(User $user, $take = 20)
    {
        return static::where('user_id', $user->id)
                     ->latest()
                     ->take($take)
                     ->with('subject')
                     ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function subject()
    {
        return $this->morphTo();
    }
}
