<?php

namespace App\Models;

use App\Traits\Favorable;
use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Reply extends Model
{
    use Favorable, RecordsActivity;

    protected static $recordEvents = ['created'];

    protected $with    = ['author'];
    protected $guarded = [];
    protected $appends = ['favorited'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @param int $minutes
     * @return boolean
     */
    public function wasJustCreated($minutes = 1)
    {
        return $this->created_at->gt(now()->subMinutes($minutes));
    }

    /**
     * @return string
     */
    public function path()
    {
        return $this->thread->path() . '#reply-' . $this->id;
    }

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection|Collection
     */
    public function mentionedUsers()
    {
        preg_match_all('/@([\w\-]+)/', $this->body, $matches);

        return $matches[1]
            ? User::whereIn('name', $matches[1])->get()
            : new Collection;
    }

    /**
     * @param $body
     */
    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = preg_replace('/@([\w\-]+)/', '<a href="/profiles/$1">$0</a>', $body);
    }
}