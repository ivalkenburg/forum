<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['name', 'email', 'password', 'avatar_path'];
    protected $hidden   = ['password', 'remember_token', 'email'];
    protected $casts    = ['is_admin' => 'boolean'];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function threads()
    {
        return $this->hasMany(Thread::class)->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Illuminate\Database\Query\Builder
     */
    public function lastThread()
    {
        return $this->hasOne(Thread::class)->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Illuminate\Database\Query\Builder
     */
    public function lastReply()
    {
        return $this->hasOne(Reply::class)->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * @return string
     */
    public function profile()
    {
        return route('profiles.show', $this->name);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return (bool) $this->is_admin;
    }

    /**
     * @param Model  $model
     * @param string $foreign
     * @return bool
     */
    public function owns(Model $model, $foreign = 'user_id')
    {
        return $this->id == $model->{$foreign};
    }

    /**
     * @param Thread $thread
     * @return $this
     * @throws \Exception
     */
    public function read(Thread $thread)
    {
        cache()->forever($this->tlvCacheKey($thread), now());

        return $this;
    }

    /**
     * @param Thread $thread
     * @return string
     */
    public function tlvCacheKey(Thread $thread)
    {
        return sprintf('tlv.%s.%s', $this->id, $thread->id);
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function avatar()
    {
        return $this->avatar_path ?: config('app.default_avatar_path');
    }
}
