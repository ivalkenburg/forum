<?php

namespace App\Models;

use App\Notifications\ThreadHasNewReply;
use Illuminate\Database\Eloquent\Model;

class ThreadSubscription extends Model
{
    public    $timestamps = false;
    protected $guarded    = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    /**
     * @param Reply $reply
     * @return mixed
     */
    public function notify(Reply $reply)
    {
        return $this->user->notify(new ThreadHasNewReply($this->thread, $reply));
    }
}
