<?php

namespace App\Models;

use App\Events\ThreadReceivedReply;
use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use RecordsActivity;

    protected static $recordEvents = ['created'];

    protected $with      = ['channel', 'author'];
    protected $withCount = ['replies'];
    protected $appends   = ['isSubscribed'];
    protected $guarded   = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($thread) {
            $thread->replies->each->delete();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @param array $reply
     * @return Reply
     */
    public function addReply(array $reply)
    {
        /** @var Reply $reply */
        $reply = $this->replies()->create($reply);

        event(new ThreadReceivedReply($this, $reply));

        return $reply;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    /**
     * @param null $userId
     * @return Model|ThreadSubscription
     */
    public function subscribe($userId = null)
    {
        return $this->subscriptions()->create([
            'user_id' => $userId ?: auth()->id()
        ]);
    }

    /**
     * @return bool
     */
    public function getIsSubscribedAttribute()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }

    /**
     * @param null $userId
     * @return $this
     */
    public function unsubscribe($userId = null)
    {
        return $this->subscriptions()
                    ->where('user_id', $userId ?: auth()->id())
                    ->delete();
    }

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasUpdatesFor(User $user)
    {
        return $this->updated_at > cache($user->tlvCacheKey($this));
    }

    /**
     * @param int $minutes
     * @return bool
     */
    public function wasJustCreated($minutes = 1)
    {
        return $this->created_at > now()->subMinutes($minutes);
    }

    /**
     * @param null $append
     * @return string
     */
    public function path($append = null)
    {
        $append = $append ? '/' . trim($append, '/') : '';

        return route('threads.show', ['channel' => $this->channel->slug, 'thread' => $this->id]) . $append;
    }
}