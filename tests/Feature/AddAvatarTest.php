<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AddAvatarTest extends TestCase
{
    use RefreshDatabase;

    /** @test * */
    public function only_authenticated_users_can_add_avatars()
    {
        $this->postJson('/api/profile/avatar')
             ->assertStatus(401);
    }

    /** @test * */
    public function a_valid_avatar_image_must_be_provided()
    {
        $this->signIn();

        $this->postJson('/api/profile/avatar', [
            'avatar' => 'not-an-image'
        ])->assertJsonValidationErrors('avatar');
    }

    /** @test * */
    public function a_user_may_add_an_avatar_to_their_profile()
    {
        $this->signIn();

        Storage::fake('public');

        $this->postJson('/api/profile/avatar', [
            'avatar' => $file = UploadedFile::fake()->image('avatar.jpg')
        ])->assertStatus(302);

        $this->assertEquals('avatars/' . $file->hashName(), auth()->user()->avatar_path);

        Storage::disk('public')->assertExists('avatars/' . $file->hashName());
    }
}
