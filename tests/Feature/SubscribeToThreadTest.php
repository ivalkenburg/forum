<?php

namespace Tests\Feature;

use App\Models\Thread;
use Tests\TestCase;

class SubscribeToThreadTest extends TestCase
{
    /** @test */
    public function a_user_can_subscribe_to_a_thread()
    {
        $this->signIn();

        $thread = create(Thread::class);

        $this->postJson($thread->path('subscriptions'))
             ->assertStatus(201);

        $this->assertCount(1, $thread->subscriptions);
    }

    /** @test */
    public function a_user_can_unsubscribe_from_a_thread()
    {
        $this->signIn();

        $thread = create(Thread::class);

        $thread->subscribe();

        $this->deleteJson($thread->path('subscriptions'))
             ->assertStatus(200);

        $this->assertCount(0, $thread->subscriptions);
    }
}