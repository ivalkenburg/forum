<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Models\User;
use Illuminate\Notifications\DatabaseNotification;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    /** @var User */
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->signIn();
    }

    /** @test */
    public function a_user_receives_a_notification_when_a_thread_receives_a_new_reply_that_is_not_by_the_current_user()
    {
        $thread = Create(Thread::class);

        $thread->subscribe();

        $this->assertCount(0, auth()->user()->notifications);

        $thread->addReply([
            'user_id' => auth()->id(),
            'body'    => 'Some reply'
        ]);

        $thread->addReply([
            'user_id' => create(User::class)->id,
            'body'    => 'Some reply'
        ]);

        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    /** @test */
    public function a_user_can_mark_a_notification_as_read()
    {
        create(DatabaseNotification::class);

        $this->assertCount(1, $this->user->unreadNotifications);

        $notification = $this->user->unreadNotifications->first();

        $this->deleteJson("/profiles/{$this->user->name}/notifications/$notification->id")
             ->assertStatus(200);

        $this->assertCount(0, $this->user->fresh()->unreadNotifications);
    }

    /** @test */
    public function a_user_can_retrieve_their_notifications()
    {
        create(DatabaseNotification::class);

        $this->getJson("/profiles/{$this->user->name}/notifications")
             ->assertJsonCount(1);
    }

    /** @test */
    public function a_user_can_mark_all_their_notifications_as_read()
    {
        create(DatabaseNotification::class, [], 5);

        $this->assertCount(5, $this->user->unreadNotifications);

        $this->deleteJson("/profiles/{$this->user->name}/notifications/all")
             ->assertStatus(200);

        $this->assertCount(0, $this->user->fresh()->unreadNotifications);
    }
}