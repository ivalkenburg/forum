<?php

namespace Tests\Feature;

use App\Models\Activity;
use App\Models\Favorite;
use App\Models\Reply;
use App\Models\Thread;
use Tests\TestCase;

class ParticipateInThreadsTest extends TestCase
{
    /** @var Thread */
    protected $thread;

    protected function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);
    }

    /** @test */
    public function authenticated_user_may_post_replies()
    {
        $this->signIn();

        $reply = raw(Reply::class);

        $this->postJson($this->thread->path('replies'), $reply);

        $this->assertDatabaseHas('replies', ['body' => $reply['body']]);
        $this->assertEquals(1, $this->thread->fresh()->replies_count);
    }

    /** @test */
    public function unauthenticated_users_not_post_replies()
    {
        $this->postJson($this->thread->path('/replies'), [])
             ->assertStatus(401);
    }

    /** @test */
    public function a_reply_requires_a_body()
    {
        $this->signIn();

        $reply = raw(Reply::class, ['body' => '']);

        $this->postJson($this->thread->path('replies'), $reply)
             ->assertJsonValidationErrors('body');
    }

    /** @test */
    public function unauthorized_users_cannot_delete_replies()
    {
        $reply = create(Reply::class);

        $this->deleteJson(route('replies.destroy', $reply->id))
             ->assertStatus(401);

        $this->signIn();

        $this->deleteJson(route('replies.destroy', $reply->id))
             ->assertStatus(403);
    }

    /** @test */
    public function authorized_users_can_delete_replies()
    {
        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->id()]);

        $this->postJson(route('replies.favorite', $reply->id))
             ->assertStatus(200);

        $this->deleteJson(route('replies.destroy', $reply->id))
             ->assertStatus(200);

        $this->assertCount(0, Reply::all());
        $this->assertCount(0, Activity::all());
        $this->assertCount(0, Favorite::all());
    }

    /** @test */
    public function unauthorized_users_can_not_update_replies()
    {
        $reply = create(Reply::class);

        $this->patchJson(route('replies.update', $reply->id), ['body' => 'foobar'])
             ->assertStatus(401);

        $this->signIn();

        $this->patchJson(route('replies.update', $reply->id), ['body' => 'foobar'])
             ->assertStatus(403);
    }

    /** @test */
    public function authorized_users_can_update_replies()
    {
        $updatedBody = 'Lorem ipsum dolar bill blahblah';

        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->id()]);

        $this->patchJson(route('replies.update', $reply->id), ['body' => $updatedBody])
             ->assertStatus(200);

        $this->assertDatabaseHas('replies', ['id' => $reply->id, 'body' => $updatedBody]);
    }

    /** @test * */
    public function replies_that_contain_spam_will_not_be_created()
    {
        $this->signIn();

        $reply = raw(Reply::class, ['body' => 'Yahoo Customer Support']);

        $this->postJson($this->thread->path('/replies'), $reply)
             ->assertJsonValidationErrors('body');

        $this->assertDatabaseMissing('replies', ['body' => 'Yahoo Customer Support']);
    }

    /** @test * */
    public function cannot_edit_reply_with_spam_in_body()
    {
        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->id()]);

        $this->patchJson(route('replies.update', $reply->id), ['body' => 'hello wooooooooooooorld'])
             ->assertJsonValidationErrors('body');

        $this->assertDatabaseHas('replies', ['body' => $reply->body]);
    }

    /** @test * */
    public function cannot_post_a_reply_more_then_once_per_minute()
    {
        $this->signIn();

        $reply = raw(Reply::class);

        $this->postJson($this->thread->path('/replies'), $reply)
             ->assertStatus(201);

        $this->postJson($this->thread->path('/replies'), $reply)
             ->assertJsonValidationErrors('body');
    }
}
