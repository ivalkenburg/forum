<?php

namespace Tests\Feature;

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Tests\TestCase;

class ReadThreadsTest extends TestCase
{
    /** @var Thread */
    protected $thread;

    protected function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);
    }

    /** @test */
    public function a_guest_can_browse_all_threads()
    {
        $this->get('/threads')
             ->assertSee($this->thread->title);
    }

    /** @test */
    public function a_guest_can_browse_a_single_thread()
    {
        $this->get($this->thread->path())
             ->assertSee($this->thread->title)
             ->assertSee($this->thread->body)
             ->assertSee($this->thread->author->name);
    }

    /** @test */
    public function a_guest_can_read_replies_of_a_single_thread()
    {
        $reply = create(Reply::class, ['thread_id' => $this->thread->id]);

        $this->assertDatabaseHas('replies', [
            'body'    => $reply->body,
            'user_id' => $reply->author->id
        ]);
    }

    /** @test */
    public function a_guest_can_filter_threads_by_channel()
    {
        $channel = create(Channel::class);

        $threadInChannel = create(Thread::class, ['channel_id' => $channel->id]);

        $this->get(route('threads.channel', $channel->slug))
             ->assertSee($threadInChannel->title)
             ->assertDontSee($this->thread->title);
    }

    /** @test */
    public function a_guest_can_filter_threads_by_name()
    {
        $user = create(User::class, ['name' => 'JohnDoe']);

        $threadByJohn    = create(Thread::class, ['user_id' => $user->id]);
        $threadNotByJohn = create(Thread::class);

        $this->get(route('threads.index', ['by' => $user->name]))
             ->assertSee($threadByJohn->title)
             ->assertDontSee($threadNotByJohn->title);
    }

    /** @test */
    public function a_guest_can_filter_threads_by_popularity()
    {
        create(Thread::class, [], null, function ($thread) {
            create(Reply::class, ['thread_id' => $thread->id], 1);
        });

        create(Thread::class, [], null, function ($thread) {
            create(Reply::class, ['thread_id' => $thread->id], 3);
        });

        $response = $this->getJson(route('threads.index', ['popularity' => 1]))->json();

        $this->assertEquals([3, 1, 0], array_column($response['data'], 'replies_count'));
    }

    /** @test */
    public function a_guest_can_fetch_replies_for_a_thread()
    {
        $thread = create(Thread::class, [], null, function ($thread) {
            create(Reply::class, ['thread_id' => $thread->id], 20);
        });

        $response = $this->getJson($thread->path('replies'))->json();

        $this->assertCount(10, $response['data']);
        $this->assertEquals(20, $response['total']);
    }

    /** @test */
    public function a_guest_can_filter_threads_that_are_unanswered()
    {
        $this->signIn();

        create(Thread::class, [], null, function ($thread) {
            create(Reply::class, ['thread_id' => $thread->id], 2);
        });

        $response = $this->getJson(route('threads.index', ['unanswered' => 1]))->json();

        $this->assertCount(1, $response['data']);
    }
}
