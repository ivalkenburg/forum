<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MentionUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test **/
    public function a_user_recieves_a_notification_when_mentioned_in_a_reply()
    {
        $john = create(User::class, ['name' => 'JohnDoe']);
        $jane = create(User::class, ['name' => 'JaneDoe']);

        $this->signIn($john);

        $thread = create(Thread::class);

        $reply = raw(Reply::class, ['body' => '@JaneDoe look at this']);

        $this->postJson($thread->path('/replies'), $reply);
        
        $this->assertCount(1, $jane->notifications);
    }
}
