<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Models\User;
use Tests\TestCase;

class ProfilesTest extends TestCase
{
    /** @var User */
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = create(User::class);
    }

    /** @test */
    public function a_user_has_a_profile()
    {
        $this->get(route('profiles.show', $this->user->name))
             ->assertSee($this->user->name);
    }

    /** @test */
    public function profiles_display_threads_created_by_user()
    {
        $this->signIn($this->user);

        $thread = create(Thread::class, ['user_id' => $this->user->id]);

        $this->get(route('profiles.show', $this->user->name))
             ->assertSee($thread->title);
    }
}
