<?php

namespace Tests\Feature;

use App\Models\Activity;
use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    protected function publishThread($overrides = [])
    {
        if (!auth()->check()) {
            $this->signIn();
        }

        $thread = raw(Thread::class, $overrides);

        return $this->post(route('threads.store'), $thread);
    }

    /** @test */
    public function authenticated_users_can_create_threads()
    {
        $this->signIn();

        $thread = raw(Thread::class);

        $response = $this->post(route('threads.store'), $thread);

        $this->get($response->headers->get('Location'))
             ->assertSee($thread['title'])
             ->assertSee($thread['body']);
    }

    /** @test */
    public function unauthenticated_users_can_not_create_threads()
    {
        $this->post(route('threads.store'))
             ->assertRedirect(route('login'));

        $this->get(route('threads.create'))
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_thread_requires_a_title()
    {
        $this->publishThread(['title' => ''])
             ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_thread_requires_a_body()
    {
        $this->publishThread(['body' => ''])
             ->assertSessionHasErrors('body');
    }

    /** @test */
    public function a_thread_requires_a_valid_channel_id()
    {
        create(Channel::class, [], 3);

        $this->publishThread(['channel_id' => null])
             ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 999])
             ->assertSessionHasErrors('channel_id');
    }

    /** @test */
    public function authorized_users_can_delete_threads()
    {
        $this->signIn();

        $thread = create(Thread::class, ['user_id' => auth()->id()]);
        $reply  = create(Reply::class, ['thread_id' => $thread->id]);

        $this->post(route('replies.favorite', $reply->id));

        $this->delete($thread->path())
             ->assertRedirect(route('threads.index'));

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertDatabaseMissing('favorites', ['user_id', auth()->id(), 'favorited_id' => $reply->id, 'favorited_type' => get_class($reply)]);

        $this->assertEquals(0, Activity::count());
    }

    /** @test */
    public function unauthorized_users_can_not_delete_threads()
    {
        $thread = create(Thread::class);

        $this->delete($thread->path())
             ->assertRedirect(route('login'));

        $this->signIn();

        $this->delete($thread->path())
             ->assertStatus(403);
    }

    /** @test */
    public function an_admin_can_delete_any_thread()
    {
        $this->signIn(create(User::class, ['is_admin' => true]));

        $thread = create(Thread::class);

        $this->delete($thread->path())
             ->assertRedirect(route('threads.index'));

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
    }

    /** @test * */
    public function cannot_create_a_thread_that_contains_spam()
    {
        $this->publishThread(['body' => 'helloooooooooooo world'])
             ->assertSessionHasErrors('body');

        $this->publishThread(['title' => 'yahoo customer support'])
             ->assertSessionHasErrors('title');
    }

    /** @test * */
    public function cannot_create_a_thread_more_then_once_per_minute()
    {
        $this->publishThread();

        $this->publishThread()
             ->assertSessionHasErrors('body');
    }
}