<?php

namespace Tests\Feature;

use App\Models\Reply;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    /** @test */
    public function a_guest_can_not_favorite_a_reply()
    {
        $this->post(route('replies.favorite', 1))
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function an_authenticated_user_can_toggle_a_favorited_reply()
    {
        $this->signIn();

        $reply = create(Reply::class);

        $this->post(route('replies.favorite', $reply->id));
        $this->assertCount(1, $reply->favorites);

        $this->post(route('replies.favorite', $reply->id));
        $this->assertCount(0, $reply->fresh()->favorites);
    }
}