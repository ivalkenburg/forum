<?php

if (!function_exists('make')) {
    function make($model, $overrides = [], $amount = null) {
        return factory($model, $amount)->make($overrides);
    }
}

if (!function_exists('create')) {
    function create($model, $overrides = [], $amount = null, $callback = null) {

        $model = factory($model, $amount)->create($overrides);

        if (is_callable($callback)) {
            if ($model instanceof \Illuminate\Support\Collection) {
                $model->each($callback);
            } else {
                $callback($model);
            }
        }

        return $model;
    }
}

if (!function_exists('raw')) {
    function raw($model, $overrides = [], $amount = null) {
        return factory($model, $amount)->raw($overrides);
    }
}