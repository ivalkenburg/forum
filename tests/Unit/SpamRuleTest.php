<?php

namespace Tests\Unit;

use App\Rules\IsNotSpam;
use Tests\TestCase;

class SpamRuleTest extends TestCase
{
    protected $rule;

    protected function setUp()
    {
        $this->rule = new IsNotSpam;
    }

    /** @test */
    public function it_will_fail_to_pass_spam()
    {
        $this->assertFalse($this->rule->passes('foobar', 'yahoo customer support'));
        $this->assertFalse($this->rule->passes('foobar', 'hello wwwwwwwwworld'));
    }

    /** @test **/
    public function it_will_pass_valid_text()
    {
        $this->assertTrue($this->rule->passes('foobar', 'not hello world'));
    }
}
