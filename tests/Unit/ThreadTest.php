<?php

namespace Tests\Unit;

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use App\Notifications\ThreadHasNewReply;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    /** @var Thread */
    protected $thread;

    protected function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);
    }

    /** @test */
    public function it_has_many_replies()
    {
        $reply = create(Reply::class, ['thread_id' => $this->thread->id]);

        $this->assertInstanceOf(Collection::class, $this->thread->replies);
        $this->assertTrue($this->thread->replies->contains($reply));
    }

    /** @test */
    public function it_belongs_to_an_author()
    {
        $this->assertInstanceOf(User::class, $this->thread->author);
    }

    /** @test */
    public function it_can_add_a_reply()
    {
        $this->thread->addReply([
            'user_id' => 1,
            'body'    => 'Foobar'
        ]);

        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function it_belongs_to_a_channel()
    {
        $this->assertInstanceOf(Channel::class, $this->thread->channel);
    }

    /** @test */
    public function it_can_make_a_url_path()
    {
        $this->assertEquals(route('threads.show', ['channel' => $this->thread->channel->slug, 'thread' => $this->thread->id]), $this->thread->path());
    }

    /** @test */
    public function it_can_be_subscribed_to()
    {
        $this->thread->subscribe($userId = 1);

        $this->assertEquals(
            1, $this->thread->subscriptions()->where('user_id', $userId)->count()
        );
    }

    /** @test */
    public function it_can_unsubscribed_from()
    {
        $this->thread->subscribe($userId = 1);

        $this->assertEquals(
            1, $this->thread->subscriptions()->where('user_id', $userId)->count()
        );

        $this->thread->unsubscribe($userId);

        $this->assertEquals(
            0, $this->thread->subscriptions()->where('user_id', $userId)->count()
        );
    }

    /** @test */
    public function it_can_return_whether_the_user_is_subscribed_to_it()
    {
        $this->signIn();

        $this->assertFalse($this->thread->isSubscribed);

        $this->thread->subscribe();

        $this->assertTrue($this->thread->isSubscribed);
    }

    /** @test * */
    public function it_can_notify_subscribers_when_a_reply_is_added()
    {
        Notification::fake();

        $user = $this->signIn();

        $this->thread->subscribe();

        $this->thread->addReply([
            'user_id' => create(User::class)->id,
            'body'    => 'Foobar'
        ]);

        Notification::assertSentTo($user, ThreadHasNewReply::class);
    }

    /** @test * */
    public function it_can_check_if_the_authenticated_user_has_read_all_replies()
    {
        $user = $this->signIn();

        $this->assertTrue($this->thread->hasUpdatesFor($user));

        $user->read($this->thread);

        $this->assertFalse($this->thread->hasUpdatesFor($user));
    }

    /** @test **/
    public function it_knows_if_it_was_just_created()
    {
        $this->assertTrue($this->thread->wasJustCreated());

        Carbon::setTestNow(now()->addMinutes(2));

        $this->assertFalse($this->thread->wasJustCreated());
    }
}
