<?php

namespace Tests\Unit;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Support\Collection;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @var User */
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = create(User::class);
    }

    /** @test */
    public function it_has_many_threads()
    {
        $thread = create(Thread::class, ['user_id' => $this->user->id]);

        $this->assertInstanceOf(Collection::class, $this->user->threads);
        $this->assertTrue($this->user->threads->contains($thread));
    }

    /** @test */
    public function it_has_many_replies()
    {
        $reply = create(Reply::class, ['user_id' => $this->user->id]);

        $this->assertInstanceOf(Collection::class, $this->user->replies);
        $this->assertTrue($this->user->replies->contains($reply));
    }

    /** @test */
    public function it_has_many_activities()
    {
        $this->signIn($this->user);

        $thread = create(Thread::class, ['user_id' => $this->user->id]);

        $this->assertInstanceOf(Collection::class, $this->user->activities);
        $this->assertEquals($thread->id, $this->user->activities->first()->subject->id);
    }

    /** @test */
    public function it_has_many_favorites()
    {
        $this->signIn($this->user);

        create(Reply::class)->favorite();

        $this->assertInstanceOf(Collection::class, $this->user->favorites);
        $this->assertCount(1, $this->user->favorites);
    }

    /** @test */
    public function it_can_make_a_profile_url_path()
    {
        $this->assertEquals(route('profiles.show', $this->user->name), $this->user->profile());
    }

    /** @test **/
    public function it_can_fetch_their_most_recent_reply()
    {
        $reply = create(Reply::class, ['user_id' => $this->user->id]);

        $this->assertEquals($reply->id, $this->user->lastReply->id);
    }

    /** @test **/
    public function it_can_fetch_their_most_recent_thread()
    {
        $thread = create(Thread::class, ['user_id' => $this->user->id]);

        $this->assertEquals($thread->id, $this->user->lastThread->id);
    }

    /** @test **/
    public function it_can_determine_its_avatar_path()
    {
        $this->assertEquals(config('app.default_avatar_path'), $this->user->avatar());

        $this->user->avatar_path = 'avatars/me.jpg';

        $this->assertEquals('avatars/me.jpg', $this->user->avatar());
    }
}
