<?php

namespace Tests\Unit;

use App\Models\Channel;
use App\Models\Thread;
use Illuminate\Support\Collection;
use Tests\TestCase;

class ChannelTest extends TestCase
{
    /** @var Channel */
    protected $channel;

    protected function setUp()
    {
        parent::setUp();

        $this->channel = create(Channel::class);
    }

    /** @test */
    public function it_has_many_threads()
    {
        $thread = create(Thread::class, ['channel_id' => $this->channel->id]);

        $this->assertInstanceOf(Collection::class, $this->channel->threads);
        $this->assertTrue($this->channel->threads->contains($thread));
    }

    /** @test */
    public function it_can_make_a_url_path()
    {
        $this->assertEquals(route('threads.channel', $this->channel->slug), $this->channel->path());
    }
}
