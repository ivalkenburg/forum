<?php

namespace Tests\Unit;

use App\Models\Activity;
use App\Models\Reply;
use App\Models\Thread;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    /** @test */
    public function it_records_activity_when_a_thread_is_created()
    {
        $this->signIn();

        $thread = create(Thread::class, ['user_id' => auth()->id()]);

        $this->assertDatabaseHas('activities', [
            'type'         => 'thread_created',
            'user_id'      => auth()->id(),
            'subject_id'   => $thread->id,
            'subject_type' => get_class($thread)
        ]);

        $activity = Activity::first();

        $this->assertEquals($activity->subject->id, $thread->id);
    }

    /** @test */
    public function it_records_activity_when_a_reply_is_created()
    {
        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->id()]);

        $this->assertDatabaseHas('activities', [
            'type'         => 'reply_created',
            'user_id'      => auth()->id(),
            'subject_id'   => $reply->id,
            'subject_type' => get_class($reply)
        ]);

        $activity = Activity::first();

        $this->assertEquals($activity->subject->id, $reply->id);
    }

    /** @test */
    public function it_fetches_a_feed_for_a_user()
    {
        $this->signIn();

        create(Thread::class, ['user_id' => auth()->id()], 2);

        $feed = Activity::feed(auth()->user());

        $this->assertCount(2, $feed);
    }
}
