<?php

namespace Tests\Unit;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class ReplyTest extends TestCase
{
    /** @var Reply */
    protected $reply;

    protected function setUp()
    {
        parent::setUp();

        $this->reply = create(Reply::class);
    }

    /** @test */
    public function it_belongs_to_an_author()
    {
        $this->assertInstanceOf(User::class, $this->reply->author);
    }

    /** @test */
    public function it_belongs_to_a_thread()
    {
        $this->assertInstanceOf(Thread::class, $this->reply->thread);
    }

    /** @test **/
    public function it_knows_if_it_was_just_created()
    {
        $this->assertTrue($this->reply->wasJustCreated());

        Carbon::setTestNow(now()->addMinutes(2));

        $this->assertFalse($this->reply->wasJustCreated());
    }

    /** @test **/
    public function it_knows_which_users_were_mentioned()
    {
        $abby = create(User::class, ['name' => 'AbbyDoe']);
        $jane = create(User::class, ['name' => 'JaneDoe']);
        $john = create(User::class, ['name' => 'JohnDoe']);

        $reply = create(Reply::class, ['body' => 'Hey @JaneDoe and @JohnDoe, check this out!']);

        $this->assertTrue($reply->mentionedUsers()->contains($jane));
        $this->assertTrue($reply->mentionedUsers()->contains($john));
        $this->assertFalse($reply->mentionedUsers()->contains($abby));
    }

    /** @test **/
    public function it_replaces_mentioned_users_with_a_link_to_their_profiles()
    {
        $reply = create(Reply::class, ['body' => 'Hello @JaneDoe.']);

        $this->assertEquals('Hello <a href="/profiles/JaneDoe">@JaneDoe</a>.', $reply->body);
    }
}
