<?php

namespace Tests\Unit;

use App\Libraries\Spam\Spam;
use App\Libraries\Spam\SpamDetectedException;
use Tests\TestCase;

class SpamTest extends TestCase
{
    /** @var Spam */
    protected $spam;

    protected function setUp()
    {
        $this->spam = new Spam;

        parent::setUp();
    }

    /** @test **/
    public function it_passes_valid_text()
    {
        $this->assertFalse($this->spam->detect('innocent text'));
    }

    /** @test **/
    public function it_checks_for_invalid_keywords_by_normal_string()
    {
        $this->expectException(SpamDetectedException::class);

        $this->spam->detect('yahoo customer support');
    }

    /** @test **/
    public function it_checks_invalid_keywords_by_regex()
    {
        $this->expectException(SpamDetectedException::class);

        $this->spam->detect('hello world');
    }

    /** @test **/
    public function it_checks_for_excessive_repeating_characters()
    {
        $this->expectException(SpamDetectedException::class);

        $this->spam->detect('Hello Worrrrrrrrrrrrrrrrrrld');
    }
}
