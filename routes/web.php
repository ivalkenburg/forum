<?php

Route::redirect('/', '/threads')->name('home');

Auth::routes();

Route::get('/threads', 'ThreadController@index')->name('threads.index');
Route::get('/threads/create', 'ThreadController@create')->name('threads.create');
Route::get('/threads/{channel}', 'ThreadController@index')->name('threads.channel');
Route::post('/threads/{channel}/{thread}/subscriptions', 'ThreadSubscriptionController@store')->name('threads.subscriptions');
Route::delete('/threads/{channel}/{thread}/subscriptions', 'ThreadSubscriptionController@destroy');
Route::get('/threads/{channel}/{thread}', 'ThreadController@show')->name('threads.show');
Route::delete('/threads/{channel}/{thread}', 'ThreadController@destroy')->name('threads.destroy');
Route::post('/threads', 'ThreadController@store')->name('threads.store');
Route::get('/threads/{channel}/{thread}/replies', 'ReplyController@index')->name('replies.index');
Route::post('/threads/{channel}/{thread}/replies', 'ReplyController@store')->name('replies.store');

Route::post('/replies/{reply}/favorite', 'ReplyController@favorite')->name('replies.favorite');
Route::delete('/replies/{reply}', 'ReplyController@destroy')->name('replies.destroy');
Route::patch('/replies/{reply}', 'ReplyController@update')->name('replies.update');

Route::delete('profiles/{user}/notifications/all', 'UserNotificationController@destroyAll');
Route::delete('/profiles/{user}/notifications/{notification}', 'UserNotificationController@destroy');
Route::get('/profiles/{user}/notifications', 'UserNotificationController@index');
Route::get('/profiles/{user}', 'ProfileController@show')->name('profiles.show');

Route::post('/api/profile/avatar', 'Api\UserAvatarController@store')->name('profile.avatar.store');